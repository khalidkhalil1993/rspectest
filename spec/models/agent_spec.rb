describe Agent, '#favorite_gadget' do
 
    it 'returns one item, the favorite gadget of the agent ' do
      expect(agent.favorite_gadget).to eq 'Walther PPK'
    end
   
  end