require 'rails_helper'

# RSpec.describe DummyModel, type: :model do
#   pending "add some examples to (or delete) #{__FILE__}"
# end



describe DummyModel, '#name,#number' do
 
  it 'returns two item, the name and test_number of the dummy model ' do
  # Setup
    dummymodel = DummyModel.create(name: 'James Bond',test_number: 20)
 
  # Exercise    
    name = dummymodel.name
    test_number = dummymodel.test_number

  # Verify
  # expect(name).to eq 'Walther PPK'

    expect(name).not_to eq 'Walther PPK'
    expect(test_number).to eq 20

    # expect(test_number).not_to eq 20

  # Teardown is for now mostly handled by RSpec itself
  end
 
end

